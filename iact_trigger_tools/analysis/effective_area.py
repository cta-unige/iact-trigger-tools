import logging
import numpy as np
import pandas as pd
import sys
from multiprocessing import Pool
from scipy import stats

logger = logging.getLogger(__name__)

# Error calculation of trigger rate based on eqs 8, 9 in
# http://www.pp.rhul.ac.uk/~cowan/atlas/ErrorBars.pdf
# - classical error of binomial distribution in not suggested for efficiencies close to 0 of 1
def frequentist_confidence_interval(p, n, alpha=0.159, beta=0.159):
    '''
    Calculate frequentist confidence interval between 1-alpha (lower) and 1-beta (upper)
    for efficiency-like distribution (p/n)

    :param float p: Number of events passing selection
    :param float n: Total number of events
    :param float alpha: Confidence level for lower limit
    :param float beta: Confidence level for upper limit

    :return: (low, up) confidence interval
    '''

    f1 = stats.f.ppf(alpha, dfn=2*p, dfd=2*(n-p+1))
    f2 = stats.f.ppf(1-beta, dfn=2*(p+1), dfd=2*(n-p))
    low = p * f1 / (n - p + 1 + p * f1)
    up = (p + 1) * f2 / (n - p + (p + 1) * f2)
    return (low, up)


def calculate_impact(event, tel_position):
    #Calculate event direction cosines
    cx = np.cos(event['Altitude']) * np.cos(event['Azimuth'])
    cy = np.cos(event['Altitude']) * np.sin(event['Azimuth'])
    cz = np.sin(event['Altitude'])

    direction = np.array([cx, cy,cz]).T

    impact_point = np.array([event['X'], event['Y'], 0])
    impact_vector = np.cross((impact_point-tel_position), direction)

    return np.sqrt(impact_vector.dot(impact_vector))


def calculate_delta(event, tel_al, tel_az):
    '''
    Calculate obsevation angle of a primary particle

    :param pandas.DataFrame event: Dataframe row containing shower altitude and azimuth in radians
    :param float tel_al: Telescope pointing altitude in radians
    :param float tel_az: Telescope pointing azimuth in radians
    '''
    return np.arccos(np.sin(event['Altitude']) * np.sin(tel_al) +
                     np.cos(event['Altitude']) * np.cos(tel_al) *
                     np.cos(abs(tel_az - event['Azimuth'])))


def aeff_func_log(log_e, a, b, c, d):
    '''
    Fit function for effective area
    '''
    return np.log10(a) + b*log_e - np.log10(1 + (10**(log_e)/c)**d)


def analyze(metadata, data, binning, store_probability=None):
    '''
    Compute the differential trigger probability and effective area

    :param dict metadata: Run metadata
    :param pandas.DataFrame data: Event data in trigger analysis format
    :param ndarray binning: An array of bin edges along: energy, impact parameter, incident angle
    (last is only for diffuse production)

    :return: effective area, low error, up error (1 sigma bend)

    '''
    logging.debug('Binning:\n%s', binning)
    n_simulated_events = len(data.index)
    logging.debug('Number of simulated events: %s', n_simulated_events)
    n_triggered_events = len(data.dropna().index)
    logging.debug('Number of triggered events: %s', n_triggered_events)

    data['Delta'] = data.apply(calculate_delta, tel_al=metadata['TELESCOPE POINTING'][1],
                               tel_az=metadata['TELESCOPE POINTING'][0], axis=1)
    data['Impact'] = data.apply(calculate_impact, tel_position=metadata['TELESCOPE POSITIONS'][0],
                                axis=1)
    # Calculate the differential trigger probability as a function of energy, impact parameter and
    # primary particle incident angle (if applicable, i.e. not a point-source observation)
    simulated_events = np.array([data['Energy'], data['Impact'], data['Delta']])
    triggered_data = data.dropna()
    triggered_events = np.array([triggered_data['Energy'], triggered_data['Impact'], triggered_data['Delta']])
    logging.debug('Triggered events:\n%s', triggered_data[['Energy', 'Impact', 'Delta']].head(50))
    simulated_hist, _ = np.histogramdd(simulated_events.T, binning)
    triggered_hist, _ = np.histogramdd(triggered_events.T, binning)
    simulated_mask = simulated_hist > 0
    trigger_probability = np.zeros_like(simulated_hist)
    trigger_probability[simulated_mask] = triggered_hist[simulated_mask] / simulated_hist[simulated_mask]
    logger.debug('Trigger probability:\n%s', trigger_probability)
    trigger_probability_low = np.zeros_like(simulated_hist)
    trigger_probability_up = np.zeros_like(simulated_hist)
    trigger_probability_low, trigger_probablity_up = frequentist_confidence_interval(triggered_hist,
                                                                                     simulated_hist)

    if store_probability is not None:

        np.savez(store_probability, binning=binning,
                 differential_trigger_probability=trigger_probability,
                 differential_trigger_probability_low=trigger_probability_low,
                 differential_trigger_probability_high=trigger_probability_up,
                )

    # Calculate the effective area as a function of energy
    r = 0.5 * (binning[1][1:] + binning[1][:-1]) # get impact parameter bin centers
    dr = (binning[1][1:] - binning[1][:-1]) # get impact parameter bin widths
    rdr = r * dr
    if binning.shape[0] > 2:
        omega_edges = 2 * np.pi * (1 - np.cos(binning[2]))
        domega = (omega_edges[1:] - omega_edges[:-1]).reshape([1, 1, -1])
        rdr = rdr.reshape([1, -1, 1])
        eff_area = 2 * np.pi * np.sum(trigger_probability * rdr * domega, axis=(1, 2))
        eff_area_err_lo = 2 * np.pi * np.sqrt(np.nansum(((trigger_probability_low - trigger_probability) * rdr * domega)**2, axis=(1, 2)))
        eff_area_err_up = 2 * np.pi * np.sqrt(np.nansum(((trigger_probability_up - trigger_probability) * rdr * domega)**2, axis=(1, 2)))
    else:
        rdr = rdr.reshape([1, -1])
        eff_area = 2 * np.pi * np.sum(trigger_probability * rdr, axis=1)
        eff_area_err_lo = 2 * np.pi * np.sqrt(np.nansum(((trigger_probability_low - trigger_probability) * rdr)**2, axis=1))
        eff_area_err_up = 2 * np.pi * np.sqrt(np.nansum(((trigger_probability_up - trigger_probability) * rdr)**2, axis=1))

    return eff_area, eff_area_err_lo, eff_area_err_up
