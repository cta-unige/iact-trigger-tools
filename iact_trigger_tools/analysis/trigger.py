import astropy.units as u
import logging
import numpy as np
import matplotlib.pyplot as plt
import sys

from pyirf.spectral import (
        IRFDOC_PROTON_SPECTRUM, PDG_ALL_PARTICLE, calculate_event_weights, PowerLaw
)
from pyirf.simulations import SimulatedEventsInfo

from scipy import optimize

import iact_trigger_tools.analysis.effective_area as ea

logger = logging.getLogger(__name__)


def plot_effective_area(bin_centers, bin_centers_smooth, effective_area_triplet, mask, fit, save_file=None):
    '''
    '''
    fig = plt.figure(figsize=(10,8))
    plt.errorbar(bin_centers, effective_area_triplet[0],
                 yerr=[effective_area_triplet[1], effective_area_triplet[2]],
                 fmt='.', color='gray', label="simulation")
    plt.errorbar(bin_centers[mask], effective_area_triplet[0][mask],
                 yerr=[effective_area_triplet[1][mask], effective_area_triplet[2][mask]],
                 fmt='.', color='black', label="simulation, points fitted")
    plt.plot(bin_centers_smooth, fit, 'r-', label="Fit")
    plt.title('Effective area')
    plt.ylabel('Effective area [m$^2$sr]')
    plt.xlabel('E [TeV]')
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    if save_file is not None:
        plt.savefig(save_file, dpi=100)
    plt.close(fig)


def differential_rate(data, config):
    '''
    Calculate the trigger rate as a function of incident particle energy

    :param dict data: Dictionary containing metadata and events data frame
    :param dict config: Parsed toml config

    :return: ndarray
    '''
    metadata = data['metadata']
    threshold = metadata['ASUM THRESHOLD']
    binning_list = []
    for dimension, binning_1d in sorted(config['binning'].items()):  # Ensure that energy binning goes first
        logger.debug(f'Binning parameters for {dimension}: {binning_1d}')
        if binning_1d['log']:
            logger.debug('Setting log space')
            binning_list.append(np.logspace(binning_1d['min'],
                                            binning_1d['max'],
                                            binning_1d['n_bins'] + 1))
        else:
            logger.debug('Setting linear space')
            binning_list.append(np.linspace(binning_1d['min'],
                                            binning_1d['max'],
                                            binning_1d['n_bins'] + 1))
    binning = np.array(binning_list, dtype=object)
    eff_area_t =  ea.analyze(metadata, data['data'], binning,
                             f"{config['output_dir']}/trigger_probability_threshold_{threshold}.npz")
    logger.debug('Effective area:\n%s', eff_area_t)
    # mask low energy part of the spectrum where effective area looks strange
    mask = np.logical_and(eff_area_t[0] > config['selection']['min_effective_area'],
                          binning[0][1:] < config['selection']['min_energy'])
    logger.debug('Effective area mask:\n%s', mask)
    eff_area_t_masked = tuple(map(lambda x: x[mask], eff_area_t))
    logger.debug('Effective area masked:\n%s', eff_area_t_masked)
    energy_center_values = 0.5 * (binning[0][1:] + binning[0][:-1])
    energy_center_values_masked = energy_center_values[mask]
    log_sigma = np.log10(eff_area_t_masked[2] - eff_area_t_masked[1])
    try:
        fit, cov = optimize.curve_fit(ea.aeff_func_log, np.log10(energy_center_values_masked),
                                      np.log10(eff_area_t_masked[0]), sigma=log_sigma,
                                      p0=config['fit']['initial_parameters'])
        logger.info('Fit values: :%s', fit)
        np.savetxt(f"{config['output_dir']}/fit_results_threshold_{threshold}.txt", fit)
    except RuntimeError:
        fit = None
        logger.error('Effective area fit failed! Exiting...')
        sys.exit(1)

    energy_smooth = np.logspace(config['binning']['energy']['min'],
                                config['binning']['energy']['max'], 100)

    energy_center_values_smooth = 0.5 * (energy_smooth[1:] + energy_smooth[:-1])

    plot_effective_area(energy_center_values,
                        energy_center_values_smooth,
                        eff_area_t,
                        mask,
                        pow(10, ea.aeff_func_log(np.log10(energy_center_values_smooth), *fit)),
                        f"{config['output_dir']}/effective_area_threshold_{threshold}.png")

    de = (energy_smooth[1:] - energy_smooth[:-1])

    spectrum = config['spectrum']['norm'] * pow(energy_center_values_smooth, config['spectrum']['index'])

    return de, spectrum * pow(10, ea.aeff_func_log(np.log10(energy_center_values_smooth), *fit))


def reweight_rate(data, config):
    '''
    Calculate the trigger rate through reweighting generated spectrum to the observed one

    :param dict data: Dictionary containing metadata and events data frame
    :param dict config: Parsed toml config

    :return: int
    '''

    sim_events = data['data']
    logger.debug('Number of simulated events: %s', len(sim_events.index))
    simulated_events = SimulatedEventsInfo(
        n_showers=len(sim_events.index),
        energy_min=config['simulated_events_info']['energy_min'] * u.TeV,
        energy_max=config['simulated_events_info']['energy_max'] * u.TeV,
        max_impact=config['simulated_events_info']['max_impact'] * u.km,
        spectral_index=config['simulated_events_info']['spectral_index'],
        viewcone=config['simulated_events_info']['viewcone'] * u.deg,
    )
    simulated_spectrum = PowerLaw.from_simulation(simulated_events, obstime=1 * u.s)

    energies = np.geomspace(config['simulated_events_info']['energy_min'] * u.TeV,
                            config['simulated_events_info']['energy_max'] * u.TeV,
                            500)

    logger.debug('Energies:\n%s', energies)
    # weights are event numbers for 1 second of observation time
    weights = calculate_event_weights(
            energies,
            target_spectrum=IRFDOC_PROTON_SPECTRUM,  # or use the PDG_ALL_PARTICLE
            simulated_spectrum=simulated_spectrum,
    )
    logger.debug('Weights:\n%s', weights)
    triggered_events = sim_events.dropna().copy()
    triggered_events['weight_index'] = np.digitize(np.asarray(triggered_events['Energy']) * u.TeV, energies)
    triggered_events['weight'] = triggered_events.apply(lambda x: weights[x['weight_index']], axis=1)
    rate = np.sum(triggered_events['weight'])
    return rate


