import copy
import itertools
import logging
import h5py
import pandas as pd
import pickle
import re

from eventio import EventIOFile, SimTelFile
from eventio.search_utils import yield_all_subobjects
from eventio.simtel.objects import History, HistoryConfig
from multiprocessing import Pool

# **********************GLOBAL VARIABLES*********************

logger = logging.getLogger(__name__)

# ***********************************************************


def parse_cfg_bytestring(bytestring):
    '''
    Parse configuration as read by eventio

    :param bytes bytestring: A ``Bytes`` object with configuration data for one parameter

    :return: Tuple in form ``('parameter_name', 'value')``
    '''
    line_decoded = bytestring.decode('utf-8').rstrip()
    if 'ECHO' in line_decoded or '#' in line_decoded:
        return None
    line_list = line_decoded.split('%', 1)[0]  # drop comment
    res = re.sub(' +', ' ', line_list).strip().split(' ', 1)  # remove extra whitespaces and split
    return (res[0].upper(), res[1])


def get_run_configuration(filename):
    '''
    Get current run configuration from simtel file

    :param str filename: Input file name
    :return dict: Dictionary of {'parameter_name': 'value'} according to `sim_telarray` specification
    '''
    config = {}
    with EventIOFile(filename) as f:
        for o in yield_all_subobjects(f, [History, HistoryConfig]):
            if hasattr(o, 'parse'):
                try:
                    cfg_element = parse_cfg_bytestring(o.parse()[1])
                    if cfg_element is not None:
                        config[cfg_element[0]] = cfg_element[1]
                except Exception as e:
                    logger.warning('Unexpected end of %s,\n caught exception %s', filename, e)
    return config


def convert_to_trigger_format(ifile, ofile, max_event_id=None):
    """
    Extract important for trigger studies information from sim_telarray file and store as pandas
    dataframe using pickle together with certain metadata

    Stored data format:
    1. Metadata:
        - Telescope coordinates (once per file)
        - Nightsky background rate
        - Trigger threshold
        - FADC rate
        - Number of FADC samples per event
        - Telescope positions
        - Telescope pointing
    2. Events:
        | Particle ID | Azimuth | Altitude | X | Y | Energy | Triggered Telescopes | Triggered Sectors |

        * Triggered Sectors is a dictionary: {'telescope id': [triggered sectors]}
    """

    metadata = {}
    events = {}
    run_config = get_run_configuration(ifile)
    metadata['NSB RATE'] = re.sub('[^\d\.]', '', run_config['NIGHTSKY_BACKGROUND'])
    metadata['ASUM THRESHOLD'] = int(run_config['ASUM_THRESHOLD'])
    metadata['ADC RATE'] = int(run_config['FADC_MHZ']) * 1000000  # ADC rate in Hz
    metadata['ADC BINS'] = int(run_config['FADC_BINS'])
    metadata['TELESCOPE POSITIONS'] = None
    metadata['TELESCOPE POINTING'] = None
    # initialize empty structure of event
    event = {'Particle ID': None,
             'Azimuth': None,
             'Altitude': None,
             'First Interaction Height': None,
             'X': None,
             'Y': None,
             'Energy': None,
             'Triggered Telescopes': None,  # contains list of triggered telescopes
             'Triggered Sectors': None  # contains dictionary in form {'telescope id': [triggered sectors]}
             }

    # Iterate over all simulated events, extract data
    with SimTelFile(ifile) as f:
        metadata['TELESCOPE POSITIONS'] = f.header['tel_pos']
        metadata['TELESCOPE POINTING'] = f.header['direction']
        mc_iter = f.iter_mc_events()
        try:
            for _ in itertools.islice(mc_iter, max_event_id):
                mc_shower = _['mc_shower']
                mc_event = _['mc_event']
                event['Particle ID'] = mc_shower['primary_id']
                event['Azimuth'] = mc_shower['azimuth']
                event['Altitude'] = mc_shower['altitude']
                event['First Interaction Height'] = mc_shower['h_first_int']
                event['X'] = mc_event['xcore']
                event['Y'] = mc_event['ycore']
                event['Energy'] = mc_shower['energy']
                events[str(_['event_id'])] = copy.deepcopy(event)
        except Exception as e:
            logger.error('''Caught exception:\n%s\nMost probably your file is truncated.
                         Not a big deal, we save all the events before it got truncated''', e)

    # Iterate over triggered events, fill trigger info
    with SimTelFile(ifile) as f:
        triggered_iter = f.iter_array_events()
        try:
            for _ in itertools.islice(triggered_iter, max_event_id):
                event_id = str(_['event_id'])
                events[event_id]['Triggered Telescopes'] = _['trigger_information']['triggered_telescopes']
                triggered_sectors = {}
                for telescope_id, telescope_event in _['telescope_events'].items():
                    triggered_sectors[telescope_id] = telescope_event['header']['list_trgsect']
                events[event_id]['Triggered Sectors'] = triggered_sectors
        except KeyError as e:
            if max_event_id is not None:
                pass  # We iterate over a subset of a file and in this subset there were non-triggered events, thus max_event_id > len(triggered events in the subset)
            else:
                raise KeyError(e)
        except Exception as e:
            logger.error('''Caught exception:\n%s\nMost probably your file is truncated.
                         Not a big deal, we save all the events before it got truncated''', e)
    #df = pd.DataFrame(events)
    df = pd.DataFrame.from_dict(events, orient='index')
    df.reset_index(drop=True, inplace=True)
    print(df.tail(30))
    pickle.dump({'metadata':metadata, 'data':df}, open(ofile, 'wb'))
    return


def read_pickle(ifile):
    '''
    Read a previously pickled file and return the data and metadata

    :param str ifile: Pickle file name

    :return:
        Dictionary {'metadata': dict, 'data': pandas.DataFrame}
    '''
    return pickle.load(open(ifile, 'rb'))


def load_files(filelist, n_process = 16):
    '''
    Read pickled filename, returns the pandas.DataFrame
    '''
    pool = Pool(n_process)
    content = pool.map(read_pickle, filelist)
    metadata_list = [_['metadata'] for _ in content]
    threshold_list = [_['metadata']['ASUM THRESHOLD'] for _ in content]

    if not all(x == threshold_list[0] for x in threshold_list):
        logger.error('''Metadata changes between the files,
                     probably you have messed up the different runs!''')
        sys.exit(1)

    data = pd.concat([_['data'] for _ in content])
    content = None
    return {'metadata': metadata_list[0], 'data': data}
