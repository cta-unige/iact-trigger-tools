import argparse
try:
    import importlib.resources as pkg_resources
except ImportError:
    # Try backported to PY<37 `importlib_resources`.
    import importlib_resources as pkg_resources
import logging
import numpy as np
import os
import subprocess
import sys
import toml
from iact_trigger_tools.analysis.trigger import differential_rate
from iact_trigger_tools.io.io import load_files
from iact_trigger_tools.utils import templates


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-c', '--config', help='Simulation configuration file name',
                                required=True)
    parser.add_argument('-s', '--scheduler', help='Job submission mode',
                       type=str, choices=['local', 'slurm', 'pbs'], default='local')
    return parser

def main():
    """
    Parse command line input arguments and launch the sim_runner.
    """
    parser = get_parser()
    args = parser.parse_args()
    config = {}
    try:
        config = toml.load(args.config)
    except (FileNotFoundError,
            toml.TomlDecodeError):
        logger.error("Problem during TOML configuration reading:\n%s\n Exiting...",
                     traceback.format_exc())
        sys.exit(os.EX_CONFIG)
    if args.scheduler == 'local':
        raise NotImplementedError
    elif args.scheduler == 'slurm':
        job_template = pkg_resources.read_text(templates, 'compute_per_sector_trigger_rates_slurm.job')
        with open(f"{config['output_dir']}/compute_per_sector_trigger_rates_slurm.sh", 'w') as f:
            f.write(job_template)

        for _ in config['files']:
            job_id = subprocess.check_output(['sbatch', '-o',
                                              f"{config['output_dir']}/job_threshold_{_['threshold']}.out",
                                              '-e', f"{config['output_dir']}/job_threshold_{_['threshold']}.err",
                                              f"{config['output_dir']}/compute_per_sector_trigger_rates_slurm.sh",
                                              args.config, _['filelist']],
                                             stderr=subprocess.STDOUT)

    elif args.scheduler == 'pbs':
        raise NotImplementedError
    else:
        raise Exception('Unknown job scheduler')


if __name__ == "__main__":
    main()
