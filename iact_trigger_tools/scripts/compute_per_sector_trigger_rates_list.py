import argparse
import logging
import numpy as np
import os
import sys
import toml
from iact_trigger_tools.analysis.trigger import differential_rate
from iact_trigger_tools.io.io import load_files

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-c', '--config', help='Simulation configuration file name',
                                required=True)
    required_named.add_argument('-f', '--filelist', help='Input files list',
                                required=True)
    return parser


def main():
    '''
    Compute trigger rates from a provided filelist (general config is also requred, but the
    filelists there will be ignored)
    '''
    parser = get_parser()
    args = parser.parse_args()
    config = {}
    try:
        config = toml.load(args.config)
    except (FileNotFoundError,
            toml.TomlDecodeError):
        logger.error("Problem during TOML configuration reading:\n%s\n Exiting...",
                     traceback.format_exc())
        sys.exit(os.EX_CONFIG)
    filelist = open(args.filelist, 'r').read().splitlines()
    data = load_files(filelist)

    threshold = data['metadata']['ASUM THRESHOLD']
    df = data['data']
    triggered_df = df.dropna()
    number_of_telescope_triggered = triggered_df.shape[0]
    triggered_sectors = triggered_df['Triggered Sectors'].map(lambda x: x[1])
    sector_trigger_times = np.zeros(474)
    for ts in triggered_sectors:
        for sector in ts:
            sector_trigger_times[sector] +=1
    mean = np.mean(sector_trigger_times)
    logger.info('Mean = %s', mean)
    variance = np.var(sector_trigger_times)
    sigma = np.sqrt(variance)
    logger.info('Sigma = %s', sigma)
    with open(f"{config['output_dir']}/rate_per_sector_threshold_{threshold}.txt", 'w') as f:
        f.write(f'Threshold: {threshold}\n')
        f.write(f'Camera triggered: {number_of_telescope_triggered}\n')
        f.write(f'Average sector triggered: {mean}\n')
        f.write(f'Sigma sector triggered: {sigma}\n')


if __name__ == "__main__":
    main()
