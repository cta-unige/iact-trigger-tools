import argparse
import logging
import numpy as np
import os
import sys
import toml
from iact_trigger_tools.analysis.trigger import differential_rate
from iact_trigger_tools.io.io import load_files

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-c', '--config', help='Simulation configuration file name',
                                required=True)
    required_named.add_argument('-f', '--filelist', help='Input files list',
                                required=True)
    return parser


def main():
    '''
    Compute trigger rates from a provided filelist (general config is also requred, but the
    filelists there will be ignored)
    '''
    parser = get_parser()
    args = parser.parse_args()
    config = {}
    try:
        config = toml.load(args.config)
    except (FileNotFoundError,
            toml.TomlDecodeError):
        logger.error("Problem during TOML configuration reading:\n%s\n Exiting...",
                     traceback.format_exc())
        sys.exit(os.EX_CONFIG)
    filelist = open(args.filelist, 'r').read().splitlines()
    data = load_files(filelist)
    de, diff_rate = differential_rate(data, config)
    rate = np.sum(de*diff_rate)
    threshold = data['metadata']['ASUM THRESHOLD']
    with open(f"{config['output_dir']}/rate_threshold_{threshold}.txt", 'w') as f:
        f.write(f'Threshold: {threshold}, Trigger rate: {rate}\n')


if __name__ == "__main__":
    main()
