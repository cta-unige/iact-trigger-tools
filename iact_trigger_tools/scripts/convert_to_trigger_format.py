import argparse
try:
    import importlib.resources as pkg_resources
except ImportError:
    # Try backported to PY<37 `importlib_resources`.
    import importlib_resources as pkg_resources
import os
import subprocess

from iact_trigger_tools.io.io import convert_to_trigger_format
from iact_trigger_tools.utils import templates


def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--file', help='Sim_telarray file name')
    parser.add_argument('-l', '--list', help='Txt file with sim_telarray file names, one per line')
    parser.add_argument('-o', '--output_dir', help='Output directory')
    parser.add_argument('-m', '--max_events', help='Max number of events in a (each) file to process', type=int)
    parser.add_argument('-s', '--scheduler', help='Job submission scheduler', type=str, default='slurm')
    parser.add_argument('-v', '--verbosity', help='Logging verbosity level', type=int,
                        choices=[0, 1, 2], default=1)
    return parser


def main():
    """
    Parse command line input arguments and launch the sim_runner.
    """
    parser = get_parser()
    args, leftovers = parser.parse_known_args()

    output_dir = '.'

    if args.output_dir is not None:
        output_dir = args.output_dir
        if os.path.isdir(output_dir):
            pass
        elif os.path.isfile(output_dir):
            print('Provided output directory is not a directory, but a file! Confusing!!! Exiting...')
            exit(1)
        else:
            os.makedirs(output_dir, exist_ok=True)

    if args.list is not None:
        with open(args.list, 'r') as f:
            for line in f.readlines():
                ifile = line.rstrip()
                ifile_local = ifile.split('/')[-1]
                if args.scheduler == 'slurm':
                    job_template = pkg_resources.read_text(templates, 'converter_slurm.job')

                    with open(f'{output_dir}/converter_job.sh', 'w') as f:
                        f.write(job_template)
                    job_id = subprocess.check_output(['sbatch', '-o', f'{output_dir}/{ifile_local[:-10]}.out',
                                                      '-e', f'{output_dir}/{ifile_local[:-10]}.err',
                                                      f'{output_dir}/converter_job.sh', ifile, output_dir],
                                                     stderr=subprocess.STDOUT)
                elif args.scheduler == 'pbs':
                    raise NotImplementedError
                else:
                    raise Exception('Unknown job scheduler')
    elif args.file is not None:
        ifile = args.file
        ifile_local = ifile.split('/')[-1]
        ofile = output_dir + '/' + ifile_local[:-10] + '.pickle'
        convert_to_trigger_format(ifile, ofile, args.max_events)


if __name__ == '__main__':
    main()
