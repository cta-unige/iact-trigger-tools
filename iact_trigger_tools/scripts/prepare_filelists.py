import argparse
import os


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Directory with input files')
    parser.add_argument('-o', '--output', help='Directory with output files')
    args = parser.parse_args()

    # corsika_run25_asum_threshold_960.pickle
    threshold_files_dict = {}
    try:
        for fname in os.listdir(args.input):
            if '.pickle' in fname:
                threshold = fname.split('_')[4][:-7]
                if threshold in threshold_files_dict.keys():
                    threshold_files_dict[threshold].append(f'{args.input}/{fname}')
                else:
                    threshold_files_dict[threshold] = [f'{args.input}/{fname}',]
    except Exception as e:
        print('Caught an exception!\n%s', e)

    for key, value in threshold_files_dict.items():
        with open(f'{args.output}/threshold_{key}.txt', 'a') as f:
            for _ in value:
                f.write(f'{_}\n')


if __name__ == '__main__':
    main()
