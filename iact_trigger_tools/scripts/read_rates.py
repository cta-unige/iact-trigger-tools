import argparse
import glob
import logging
import matplotlib.pyplot as plt


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

def get_parser():
    """
    Create command line options parser

    :return: Parser object
    :rtype: class `argparse.ArgumentParser`
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--idir', help='Directory with input files')
    parser.add_argument('-o', '--odir', help='Output directory')
    return parser


def main():
    '''
    Threshold: 770, Trigger rate: 1416.7037277392635
    '''
    parser = get_parser()
    args = parser.parse_args()
    thresholds = []
    rates = []
    for i in sorted(glob.glob(f'{args.idir}/rate_threshold*.txt')):
        with open(i, 'r') as f:
           vals = f.readline().split(' ')
           thresholds.append(int(vals[1][:-1])/4.8)
           rates.append(float(vals[4]))

    plt.plot(thresholds, rates, 'bo', linestyle='None', markersize = 3.0)
    plt.yscale('log')
    plt.ylim(0.1, 10**7)
    plt.savefig(f'{args.odir}/rates_plot.png')

if __name__ == "__main__":
    main()
