"""
Set of fitting utilities
"""
import numpy as np
import logging
from scipy.optimize import curve_fit
from iact_trigger_tools.utils.logger import logger


def get_fit_range(x, y):
    '''
    Calculates the the coordinates of smallest and largest derivative of 1D array-like X
    '''
    # Determine position where rates drop under 65535 (max in data)
    #start = np.argmin(y > 65535) + 1
    start = np.argmin(y > 13653330) + 1
    dx = np.diff(y[start:])
    low = np.argmin(dx)

    return start + low, start + low + np.argmax(dx[low:] > -100)

def mod_exp(x, slope, shift):
    return np.exp(slope * x + shift)

def sum_exp(x, slope_fast, shift_fast, slope_slow, shift_slow):
    '''
    Total trigger rate is driven by the nightsky background photons and diffuse protons.
    Both rates past the saturation plateau are described by exponential functions, so the total rate
    should be the sum of two exponents

    :param float x: variable
    :param float slope_fast: Slope of the NSB-describing exponent
    :param float shift_fast: Normalization of the NSB-describing exponent
    :param float slope_slow: Slope of the proton-describing exponent
    :param float shift_slow: Normalization of the proton-describing exponent

    :return: Fit function
    '''
    return np.exp(slope_fast * x + shift_fast) + np.exp(slope_slow * x + shift_slow)

def fit_trigger_rate_two_exp(rates, thresholds):
    '''
    :param array-like rates: Array of rate values for corresponding thresholds
    :param array-like thresholds: Array of threshold values

    :return: Intersection point and fit parameters of fast and slow exponents
    '''
    range_start_min, range_start_max = get_fit_range(thresholds, rates)
    # The fast slope contains about 10 points, take 7 of them for fit to be on a safe side
    fit_range_fast_exp = (range_start_min, range_start_max)
    logger.debug('Fit range for fast exp: %s:%s', thresholds[fit_range_fast_exp[0]], thresholds[fit_range_fast_exp[1]])
    # In trigger ADC units 100 is about the place where we start suffer from low statistics. Such
    # fitting range should be sufficient
    fit_range_slow_exp = (range_start_max + 15, len(thresholds))
    logger.debug('Fit range for slow exp: %s:%s',
                 thresholds[fit_range_slow_exp[0]],
                 thresholds[fit_range_slow_exp[1]-1])

    try:
        popt_fast, pcov_fast = curve_fit(mod_exp,
                                         thresholds[fit_range_fast_exp[0]:fit_range_fast_exp[1]],
                                         rates[fit_range_fast_exp[0]:fit_range_fast_exp[1]],
                                         [-0.3, 20])
    except:
        popt_fast = [None, None]
    try:
        popt_slow, pcov_slow = curve_fit(mod_exp,
                                         thresholds[fit_range_slow_exp[0]:fit_range_slow_exp[1]],
                                         rates[fit_range_slow_exp[0]:fit_range_slow_exp[1]],
                                         [-0.005, 5.0])
    except:
        popt_slow = [None, None]
    if popt_fast[0] is None or popt_slow[0] is None:
        intersection = None
        return None, None, None, None, None, None, None
    else:
        intersection = (popt_fast[1] - popt_slow[1]) / (popt_slow[0] - popt_fast[0])
        nsb_slope_err = np.sqrt(np.diag(pcov_fast))[0]
        proton_slope_err = np.sqrt(np.diag(pcov_slow))[0]
        return intersection, round(popt_fast[0], 5), round(popt_fast[1], 5), round(popt_slow[0], 5), round(popt_slow[1], 5), round(nsb_slope_err, 5), round(proton_slope_err, 5)


def fit_trigger_rate_sum_exp(rates, thresholds):
    '''
    :param array-like rates: Array of rate values for corresponding thresholds
    :param array-like thresholds: Array of threshold values

    :return: Intersection point and fit parameters of fast and slow exponents
    '''

    range_start_min, range_start_max = get_fit_range(thresholds, rates)
    logger.debug('Fit range: %s:%s', thresholds[range_start_min], thresholds[:-1])
    popt, pcov = curve_fit(sum_exp,
                           thresholds[range_start_min:],
                           rates[range_start_min:],
                           [-0.3, 20, -0.005, 5.0])
    intersection = (popt[1] - popt[3]) / (popt[2] - popt[0])

    return intersection, round(popt[0], 3), round(popt[1], 3), round(popt[2], 3), round(popt[3], 3)


def fit_trigger_rate_one_exp(rates, thresholds):
    '''
    :param array-like rates: Array of rate values for corresponding thresholds
    :param array-like thresholds: Array of threshold values

    :return: Intersection point as None (N/A), fit parameters of fast exponent, 0, 0
    '''

    range_start_min, range_start_max = get_fit_range(thresholds, rates)
    popt, pcov = curve_fit(mod_exp,
                           thresholds[range_start_min:],
                           rates[range_start_min:],
                           [-0.3, 20])
    intersection = None

    return intersection, round(popt[0], 3), round(popt[1], 3), 0, 0
