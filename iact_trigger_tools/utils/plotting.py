"""
Set of plotting utilities
"""
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import logging
from iact_trigger_tools.utils.logger import logger
from iact_trigger_tools.utils.fitting import fit_trigger_rate_two_exp, fit_trigger_rate_sum_exp, get_fit_range, mod_exp, sum_exp

def setup_plotting(no_interactive_plots=False, plotting_parameters=None):
    """
    Setup the default plotting options

    Parameters
    --------------------
    no_interactive_plots: boolean
        if true, turns off the interactive mode for plotting
    """
    plt.rcParams['figure.figsize'] = (12, 12)
    plt.rcParams['font.size'] = 20
    plt.rcParams['xtick.labelsize'] = 15
    plt.rcParams['ytick.labelsize'] = 15
    plt.rcParams['font.size'] = 15
    plt.rcParams['lines.linewidth'] = 3
    plt.rcParams['legend.numpoints'] = 1 #aby se u errorbaru a bodovych grafu nezobrazoval kazdy bod 2x
    plt.rcParams['lines.markersize'] = 15

    if no_interactive_plots:
        plt.ioff()

def plot_per_sector_rates(threshold, clean_data, title, fit_sum_exp=False):
    '''
    Plots the trigger rate per sector, their average and performs a fit to the rate.

    Parameters:
    -----------------------
    threshold: pandas.dataframe column
        The column holding the discriminator thresholds
    clean_data: pandas.dataframe
        The dataframe holding the rates after being cleaned from problematic sectors
    fit_sum_exp: boolean
        Whether to use the fit_trigger_rate_sum_exp or the fit_trigger_rate_two_exp
        fitting function

    Returns 2 subplots, one with the trigger rates per sector and their average, and
    another with the fit to the rates and the std dev band for 1,2,3 sigmas
    '''
    sector_average = clean_data.mean(axis=1)
    std_deviation = clean_data.std(axis=1)

    if fit_sum_exp:
        fit_results = fit_trigger_rate_sum_exp(sector_average, threshold)
    else:
        fit_results = fit_trigger_rate_two_exp(sector_average, threshold)

    logger.info('Average fit results: %s', fit_results)

    low, high = get_fit_range(threshold, sector_average)

    fig, axes = plt.subplots(1, 2)
    # plot trigger rates for each module and their average
    axes[0].semilogy(threshold, clean_data)
    axes[0].semilogy(threshold, sector_average, "ro", label="Average")
    # plot average trigger rate with 1,2,3 std deviation bands
    if fit_sum_exp:
        axes[1].semilogy(threshold, sum_exp(threshold, *fit_results[1:5]),
                         'k--', "Fit of average, fast")
    else:
        axes[1].semilogy(threshold, mod_exp(threshold, *fit_results[1:3]),
                         'm--', "Fit of average, fast")
        axes[1].semilogy(threshold, mod_exp(threshold, *fit_results[3:5]),
                         'm--', "Fit of averagei, slow")
        axes[1].semilogy(threshold, sum_exp(threshold, *fit_results[1:5]),
                         'k:', "Sum of two separate exponents")
    axes[1].semilogy(threshold, sector_average, "r-", label="Average")
    axes[1].axvspan(threshold[low], threshold[high], alpha=0.2, color='red')
    axes[1].axvspan(threshold[high+15], threshold[len(threshold)-1], alpha=0.2, color='red')
    for factor in range(1, 4):
        axes[1].fill_between(threshold,
                             sector_average - factor * std_deviation,
                             sector_average + factor * std_deviation,
                             alpha=0.2, label=f'Average $\pm$ {factor}$\sigma$')

    for ax in axes:
        ax.set_xlabel('Threshold, trigger DAC units')
        ax.set_ylabel('Trigger rate, Hz')
        ax.set_ylim((pow(10, -1), pow(10, 8)))
        ax.grid(True)
        ax.legend()

    fig.suptitle(title)
    plt.show()
    return fig

