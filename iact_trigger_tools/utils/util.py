import logging
import pandas as pd
import numpy as np
from iact_trigger_tools.utils.logger import logger


def get_module_pixel_map(mapping_file_name):
    '''
    Implements mapping relation between pixels and trigger modules

    :param str mapping_file_name: File with the list of pixels per each module
    :param int n_modules: Number of trigger modules in camera

    :return: Dictionary {'ModuleID':[pixel ids]}
    '''
    module_pixel_map = {}

    with open(mapping_file_name, "r") as mapping_file:
        lines = mapping_file.readlines()
        for line in lines:
            if (line.find('Pixel') != -1) and (line.find('Pixel format:') == -1):
                words = line.split()
                pixel_id = int(words[1])
                module_id = words[5]
                try:
                    tmp_pixel_list = module_pixel_map[module_id]
                    tmp_pixel_list.append(pixel_id)
                except KeyError:
                    logger.debug('Adding new module ID (%s) to the dictionary', module_id)
                    tmp_pixel_list = [pixel_id]
                module_pixel_map[module_id] = tmp_pixel_list
    return module_pixel_map


def parse_data_file(filename, n_modules=265):
    '''
    Read and parse the input file with real data

    :param str filename: Input file name
    '''
    df = pd.read_csv(filename)
    '''
    Columns are a bit mislabeled with a following pattern:
    | step | threshold | fine | module # | rate | spurious column | module # | rate | spurious column | ...
    '''
    # Clean and relabel the data
    wrong_columns = range(5, n_modules*3+5, 3)
    df.drop(df.columns[wrong_columns], axis=1, inplace=True)
    df.drop(['Step', 'fine'], 1, inplace=True)
    extra_columns = range(1, n_modules*2, 2)
    df.drop(df.columns[extra_columns], axis=1, inplace=True)
    new_column_names = ['threshold']
    new_column_names.extend([f'Module {n} rate' for n in range(1, n_modules + 1)])
    df.columns = new_column_names
    return df


def get_sector_module_map(mapping_file_name):
    '''
    Implements mapping relation between trigger sectors and modules

    :param str mapping_file_name: File with the list of modules per each trigger sector
    '''
    sector_module_map = np.genfromtxt(mapping_file_name, delimiter='\t')
    sector_module_map = np.delete(sector_module_map, 0, 0)
    sector_module_map = np.delete(sector_module_map, 0, 1)
    return sector_module_map
