import unittest

import numpy as np
from ctapipe_io_lst import load_camera_geometry
import matplotlib.pyplot as plt
import pandas as pd

from iact_trigger_tools.utils.fitting import fit_trigger_rate_two_exp, fit_trigger_rate_sum_exp, get_fit_range, mod_exp, sum_exp
from iact_trigger_tools.utils.util import get_module_pixel_map, parse_data_file

mapping_file_name = 'inputs/camera_config/camera_CTA-LST.txt'
dac_data_files = {'DAC1':'inputs/data/L1RateScanDAC1-20191218.csv',
                  'DAC2':'inputs/data/L1RateScanDAC2-20191218.csv'}

class TestTriggerTools(unittest.TestCase):

    def get_fit_results(self, df, threshold):
        fit_results_tmp = df.apply(fit_trigger_rate_two_exp,
                                   axis=0, 
                                   result_type='reduce', 
                                   args=(threshold,))
        fit_results_df = pd.DataFrame([vals for vals in fit_results_tmp], 
                                      columns=['Intersection',
                                               'NSB slope',
                                               'NSB normalization',
                                               'Proton slope',
                                               'Proton normalization',
                                               'NSB slope error',
                                               'Proton slope error'],
                                      index = fit_results_tmp.keys()) 
        return fit_results_df


    def test_fit(self):
        '''Tests fitting, cleaning of bad sectors, get_module_pixel_map'''
        excluded_modules_oscar = {'DAC1':[1, 7, 9, 20, 32, 45, 59, 74, 90, 107, 125, 142, 160, 177, 193, 208, 222, 235, 247, 248, 257, 258, 259, 260, 261, 262, 263, 264, 265], 'DAC2':[7, 107, 124, 142, 159, 160, 176, 177, 192, 193, 207, 208, 221, 222, 234, 235, 246, 247, 248, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265]}
        excluded_modules = {}
        for key, value in excluded_modules_oscar.items():
            excluded_modules[key] = value - np.ones(len(value), dtype=int)

        module_to_pixel_map = get_module_pixel_map(mapping_file_name)

        camera = load_camera_geometry()
        fig, axes = plt.subplots(1,2)
        
        for dac_index, ax in zip(excluded_modules.keys(), axes):
            mask = []
            for module in excluded_modules[dac_index]:
                mask.extend(module_to_pixel_map[str(module)])

        dac_data = {}
        for key, value in dac_data_files.items():
            dac_data[key] = parse_data_file(value)

        excluded_modules['DAC1'] = np.append(excluded_modules['DAC1'], 7)

        n_modules = 265
        threshold = dac_data['DAC1']['threshold']
        clean_data = pd.DataFrame()
        for label, df in dac_data.items():
            tmp_df = df.loc[:, df.columns != 'threshold'].copy()
            tmp_df.columns = range(n_modules)
            tmp_df.drop(excluded_modules[label], axis=1, inplace=True)
            tmp_df.columns = [f'Module_{i}_{label}' for i in tmp_df.columns]
            clean_data = pd.concat([clean_data, tmp_df], axis=1)

        fit_results_data = self.get_fit_results(clean_data, threshold)
        intersection = fit_results_data['Intersection'].mean()

        
        self.assertAlmostEqual(intersection, 56.42361176393047)


        
